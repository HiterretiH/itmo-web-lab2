const GRAPH_SIZE = 300;
const R_SIZE = 100;
var dots = [];
var x = 0,
    y = 0, 
    r = 1;

$("#input-form").bind("submit", submitForm);
$("#input-form").bind("click input", updateArea);
$("#y-input").bind("input", validateY);

$("#svg-figure").bind("click", addDot);

$("button[name=x]").bind("click", function(event) {
  x = $(this).val();
  sessionStorage.setItem("x-input", x);

  $("button[name=x]").prop("disabled", false);
  $(this).prop("disabled", true);
});

$("#y-input").bind("input", function(event) {
  y = $(this).val();
  sessionStorage.setItem("y-input", y);
});

$("input[name=r]").bind("click", function(event) {
  r = $(this).val();
  sessionStorage.setItem("r-input", r);

  $("input[name=r]").prop("checked", false);
  $(this).prop("checked", true);
});

$(document).ready(function() {
  if (sessionStorage.getItem("x-input")) {
    x = sessionStorage.getItem("x-input");
    $("button[name=x]").prop("disabled", false);
    $("button[name=x][value=" + x + "]").prop("disabled", true);
  }
  if (sessionStorage.getItem("y-input")) {
    y = sessionStorage.getItem("y-input");
    $("#y-input").val(y);
  }
  if (sessionStorage.getItem("r-input")) {
    r = sessionStorage.getItem("r-input");
    $("input[name=r]").prop("checked", false);
    $("input[name=r][value=" + r + "]").prop("checked", true);
  }

  updateArea();
});

function updateArea() {
  $("#preview-dot").attr({
    cx: x * R_SIZE / r + GRAPH_SIZE / 2, 
    cy: GRAPH_SIZE / 2 - y * R_SIZE / r,
    visibility: "visible"
  });

  $(".r-text").text(r);
  $(".neg-r-text").text(-r);
  $(".r-2-text").text(r/2);
  $(".neg-r-2-text").text(-r/2);

  for (let dot of dots) {
    dot[0].setAttribute('cx', dot[1] * R_SIZE / r + GRAPH_SIZE / 2);
    dot[0].setAttribute('cy', GRAPH_SIZE / 2 - dot[2] * R_SIZE / r);
    dot[0].setAttribute('fill', checkCollision(dot[1], dot[2]) ? 'green' : 'red');
  }
}

function validateY() {
  yInput = $("#y-input")[0];
  yText = yInput.value;
  yText = yText.replace(",", ".");
  const re = /^0(\.[0-9]+)?$|^-?[1-4](\.[0-9]+)?$|^-?5(\.0+)?$/;

  if (yText === "") {
    yInput.setCustomValidity("Заполните поле");
  }
  else if (!re.test(yText)) {
    yInput.setCustomValidity("Y должен быть числом от -5 до 5")
  }
  else {
    yInput.setCustomValidity("");
  }
}

function submitForm(event) {
  event.preventDefault();

  $.get({
    url: '',
    data: {"x-input": x, "y-input": y, "r-input": r, "time": getTime()},
    success: function(data){
      document.open();
      document.write(data);
      document.close();
    },
    error: function(error){
      console.log(error);	
    },
  });

  return false;
}

function checkCollision(dotX, dotY) {
    return (dotX <= 0 && dotY >= 0 && dotY <= r && dotX >= -r/2) ||
           (dotX <= 0 && dotY <= 0 && dotX*dotX + dotY*dotY <= r * r) ||
           (dotX >= 0 && dotY <= 0 && dotY >= dotX - r);
}

function addDot(event) {
  var mouseX = event.clientX - this.getBoundingClientRect().left;
  var mouseY = event.clientY - this.getBoundingClientRect().top;

  mouseX -= 150;
  mouseY -= 150;
  mouseX = mouseX / R_SIZE * r;
  mouseY = - mouseY / R_SIZE * r;

  var cx = mouseX * R_SIZE / r + GRAPH_SIZE / 2,
      cy = GRAPH_SIZE / 2 - mouseY * R_SIZE / r,
      fill = checkCollision(mouseX, mouseY) ? "green" : "red";

  var newDot = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    newDot.setAttribute('cx', cx);
    newDot.setAttribute('cy', cy);
    newDot.setAttribute('r', '4');
    newDot.setAttribute('fill', fill);
    newDot.setAttribute('stroke', 'white');
    newDot.setAttribute('stroke-width', '1');

  this.insertBefore(newDot, document.getElementById('preview-dot'));

  dots.push([newDot, mouseX, mouseY]);

  $.get({
    url: '',
    data: {"x-input": mouseX, "y-input": mouseY, "r-input": r, "type": "click", "time": getTime()},
    success: function(data){
      $("#result-table-body").prepend(data);
    },
    error: function(error){
      console.log(error);	
    },
  });
}

function getTime() {
  var currentDate = new Date();

  var hours = currentDate.getHours();
  var minutes = currentDate.getMinutes();

  return (hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes;
}
