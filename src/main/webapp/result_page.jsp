<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <title> Лаб 2 Иващенко </title>

        <link rel="stylesheet" href="styles/main.css">

        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    </head>
    <body>
        <div id="result-page-section">
            <h1>${hit ? "Попадание" : "Промах"}</h1>
            <!-- Фигура с координатными осями -->
            <svg id="svg-figure" height="300" width="300" xmlns="http://www.w3.org/2000/svg">
                <!-- Фигуры -->
                <polygon fill="#3399FF"
                    points="100 150 100 50 150 50 150 150" />

                <path fill="#3399FF"
                    d="M 50 150 L 150 150 L 150 250 C 150 250, 50 250, 50 150" />

                d="M 200 150 L 150 150 L 150 100 C 150 100, 200 100, 200 150" />

                <polygon fill="#3399FF"
                    points="150 150 250 150 150 250"/>

                <!-- Оси -->
                <line x1="150" y1="0" x2="150" y2="300" stroke="black" stroke-width="2" />
                <line x1="0" y1="150" x2="300" y2="150" stroke="black" stroke-width="2" />
                <polyline points="144 12 150 0 156 12"  stroke="black" stroke-width="2" fill="none" />
                <polyline points="288 144 300 150 288 156"  stroke="black" stroke-width="2" fill="none" />

                <!-- Точки на осях -->
                <line x1="50" y1="145" x2="50" y2="155" stroke="black" stroke-width="1" />
                <line x1="100" y1="145" x2="100" y2="155" stroke="black" stroke-width="1" />
                <line x1="200" y1="145" x2="200" y2="155" stroke="black" stroke-width="1" />
                <line x1="250" y1="145" x2="250" y2="155" stroke="black" stroke-width="1" />

                <line x1="145" y1="50" x2="155" y2="50" stroke="black" stroke-width="1" />
                <line x1="145" y1="100" x2="155" y2="100" stroke="black" stroke-width="1" />
                <line x1="145" y1="200" x2="155" y2="200" stroke="black" stroke-width="1" />
                <line x1="145" y1="250" x2="155" y2="250" stroke="black" stroke-width="1" />

                <!-- Точка -->
                <circle id="result-dot" cx=${cx} cy=${cy} r="4" fill=${fill} stroke="white" stroke-width="1" />

                <!-- Подписи на осях -->
                <text fill="black" x="288" y="140">X</text>
                <text fill="black" x="157" y="10">Y</text>

                <text class="r-text" fill="black" x="45" y="140">-${r}</text>
                <text class="r-2-text" fill="black" x="95" y="140">-${r_2}</text>
                <text class="r-2-text" fill="black" x="195" y="140">${r_2}</text>
                <text class="r-text" fill="black" x="245" y="140">${r}</text>

                <text class="r-text" fill="black" x="160" y="255">-${r}</text>
                <text class="r-2-text" fill="black" x="160" y="205">-${r_2}</text>
                <text class="r-2-text" fill="black" x="160" y="105">${r_2}</text>
                <text class="r-text" fill="black" x="160" y="55">${r}</text>
            </svg>

            <jsp:include page="result_section.jsp"/>

            <form action="" method="get" id="back-button-form">
                <button type="submit" id="back-button">Назад</button>
            </form>
        </div>
    </body>
</html>