<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<section id="result-section">
    <!-- Таблица с историей -->
    <table id="result-table">
        <thead id="result-table-head">
            <tr>
                <th>Время</th>
                <th>Время выполнения</th>
                <th>X</th>
                <th>Y</th>
                <th>R</th>
                <th>Попадание</th>
            </tr>
        </thead>
        <tbody id="result-table-body">
            ${tableRows}
        </tbody>
    </table>
</section>