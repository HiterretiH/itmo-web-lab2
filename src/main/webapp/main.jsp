<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <title> Лаб 2 Иващенко </title>

        <link rel="stylesheet" href="styles/main.css">

        <script defer src="scripts/main.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    </head>
    <body>
        <!-- Шапка -->
        <header>
            <p class="title">
                Лабораторная работа №2
            </p>
            <p class="info">
                Иващенко Броислав Егорович
                <br>
                Группа: P3209
                <br>
                Вариант: <a href="https://i.ibb.co/8mLvqK2/image.png" target="_blank">90830</a>
            </p>
        </header>

        <main>
            <section id="input-section">
                <!-- Фигура с координатными осями -->
                <svg id="svg-figure" height="300" width="300" xmlns="http://www.w3.org/2000/svg">
                    <!-- Фигуры -->
                    <polygon fill="#3399FF"
                        points="100 150 100 50 150 50 150 150" />

                    <path fill="#3399FF"
                        d="M 50 150 L 150 150 L 150 250 C 150 250, 50 250, 50 150" />

                    d="M 200 150 L 150 150 L 150 100 C 150 100, 200 100, 200 150" />

                    <polygon fill="#3399FF"
                        points="150 150 250 150 150 250"/>

                    <!-- Оси -->
                    <line x1="150" y1="0" x2="150" y2="300" stroke="black" stroke-width="2" />
                    <line x1="0" y1="150" x2="300" y2="150" stroke="black" stroke-width="2" />
                    <polyline points="144 12 150 0 156 12"  stroke="black" stroke-width="2" fill="none" />
                    <polyline points="288 144 300 150 288 156"  stroke="black" stroke-width="2" fill="none" />

                    <!-- Точки на осях -->
                    <line x1="50" y1="145" x2="50" y2="155" stroke="black" stroke-width="1" />
                    <line x1="100" y1="145" x2="100" y2="155" stroke="black" stroke-width="1" />
                    <line x1="200" y1="145" x2="200" y2="155" stroke="black" stroke-width="1" />
                    <line x1="250" y1="145" x2="250" y2="155" stroke="black" stroke-width="1" />

                    <line x1="145" y1="50" x2="155" y2="50" stroke="black" stroke-width="1" />
                    <line x1="145" y1="100" x2="155" y2="100" stroke="black" stroke-width="1" />
                    <line x1="145" y1="200" x2="155" y2="200" stroke="black" stroke-width="1" />
                    <line x1="145" y1="250" x2="155" y2="250" stroke="black" stroke-width="1" />

                    <!-- Точка -->
                    <circle id="preview-dot" cx="150" cy="150" r="4" fill="white" stroke="black" stroke-width="1" visibility="hidden" />

                    <!-- Подписи на осях -->
                    <text fill="black" x="288" y="140">X</text>
                    <text fill="black" x="157" y="10">Y</text>

                    <text class="neg-r-text" fill="black" x="45" y="140">-R</text>
                    <text class="neg-r-2-text" fill="black" x="95" y="140">-R/2</text>
                    <text class="r-2-text" fill="black" x="195" y="140">R/2</text>
                    <text class="r-text" fill="black" x="245" y="140">R</text>

                    <text class="neg-r-text" fill="black" x="160" y="255">-R</text>
                    <text class="neg-r-2-text" fill="black" x="160" y="205">-R/2</text>
                    <text class="r-2-text" fill="black" x="160" y="105">R/2</text>
                    <text class="r-text" fill="black" x="160" y="55">R</text>
                </svg>

                <!-- Форма -->
                <form id="input-form">
                    <!-- Ввод для X -->
                    <div id="x-container">
                        <label class="input" id="x-label">X:</label>
                        <button name="x" value="-4" type="button">-4</button>
                        <button name="x" value="-3" type="button">-3</button>
                        <button name="x" value="-2" type="button">-2</button>
                        <button name="x" value="-1" type="button">-1</button>
                        <button name="x" value="0" type="button">0</button>
                        <button name="x" value="1" type="button">1</button>
                        <button name="x" value="2" type="button">2</button>
                        <button name="x" value="3" type="button">3</button>
                        <button name="x" value="4" type="button">4</button>
                    </div>

                    <!-- Ввод для Y -->
                    <label class="input" id="y-label">
                        Y:
                        <input type="text" placeholder="[-5; 5]" name="y" id="y-input" autocomplete='off' required/>
                    </label>

                    <!-- Ввод для R -->
                    <div id="r-container">
                        <label class="input" id="r-label" for="r-input1">R:</label>
                        <input name="r" value="1" id="r-input1" type="checkbox" checked>
                        <label for="r-input1">1</label>

                        <input name="r" value="2" id="r-input2" type="checkbox">
                        <label for="r-input2">2</label>

                        <input name="r" value="3" id="r-input3" type="checkbox">
                        <label for="r-input3">3</label>

                        <input name="r" value="4" id="r-input4" type="checkbox">
                        <label for="r-input4">4</label>

                        <input name="r" value="5" id="r-input5" type="checkbox">
                        <label for="r-input5">5</label>
                    </div>

                    <div id="input-button-container">
                        <button type="submit">Отправить</input>
                    </div>
                </form>
            </section>

            <jsp:include page="result_section.jsp"/>
        </main>
    </body>

</html>