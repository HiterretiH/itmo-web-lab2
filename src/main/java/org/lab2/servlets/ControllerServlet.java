package org.lab2.servlets;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.lab2.beans.RowsBean;
import org.lab2.util.TableGenerator;

import java.io.IOException;

public class ControllerServlet extends HttpServlet {
    @Inject
    private RowsBean rows;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String x = request.getParameter("x-input");
        String y = request.getParameter("y-input");
        String r = request.getParameter("r-input");

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");

        if (x != null && y != null && r != null) {
            getServletContext().getNamedDispatcher("AreaCheckServlet").forward(request, response);
        }
        else {
            request.setAttribute("tableRows", TableGenerator.generateTable(rows));
            getServletContext().getRequestDispatcher("/main.jsp").forward(request, response);
        }
    }
}
