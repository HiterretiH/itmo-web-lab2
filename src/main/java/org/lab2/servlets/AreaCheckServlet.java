package org.lab2.servlets;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.lab2.beans.Row;
import org.lab2.beans.RowsBean;
import org.lab2.util.TableGenerator;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class AreaCheckServlet extends HttpServlet {
    @Inject
    private RowsBean rows;
    private final BigDecimal GRAPH_SIZE = new BigDecimal(300);
    private final BigDecimal R_SIZE = new BigDecimal(100);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        long startTime = System.nanoTime();

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");

        if (!validate(request)) {
            response.sendError(400, "Bad Request. Please check your input and try again.");
            return;
        }

        float x = Float.parseFloat(request.getParameter("x-input"));
        BigDecimal y = new BigDecimal(request.getParameter("y-input").replace(',', '.'));
        float r = Float.parseFloat(request.getParameter("r-input"));
        boolean hit = checkCollision(x, y, r);

        Row resultRow = new Row(
                request.getParameter("time"),
                0,
                x,
                y,
                r,
                hit
        );
        rows.addRow(resultRow);

        if (request.getParameter("type") != null && request.getParameter("type").equals("click")) {
            PrintWriter writer = response.getWriter();
            resultRow.setExecutionTime((System.nanoTime() - startTime) / 1000);
            writer.write(TableGenerator.generateRow(resultRow));
            writer.close();
        }
        else {
            request.setAttribute("cx", R_SIZE.multiply(new BigDecimal(x)).divide(new BigDecimal(r), RoundingMode.HALF_UP)
                    .add(GRAPH_SIZE).divide(new BigDecimal(2), RoundingMode.HALF_UP));
            request.setAttribute("cy", GRAPH_SIZE.divide(new BigDecimal(2), RoundingMode.HALF_UP)
                    .subtract(y.multiply(R_SIZE).divide(new BigDecimal(r), RoundingMode.HALF_UP)));
            request.setAttribute("fill", hit ? "green" : "red");
            request.setAttribute("hit", hit);
            request.setAttribute("r", r);
            request.setAttribute("r_2", r / 2);
            resultRow.setExecutionTime((System.nanoTime() - startTime) / 1000);
            request.setAttribute("tableRows", TableGenerator.generateTable(rows));
            getServletContext().getRequestDispatcher("/result_page.jsp").forward(request, response);
        }
    }

    private boolean validate(HttpServletRequest request) {
        try {
            float x = Float.parseFloat(request.getParameter("x-input"));
            BigDecimal y = new BigDecimal(request.getParameter("y-input"));
            float r = Float.parseFloat(request.getParameter("r-input"));

            return (x >= -4 && x <= 4 &&
                    y.compareTo(new BigDecimal(-5)) >= 0 && y.compareTo(new BigDecimal(5)) <= 0 &&
                    r >= 1 && r <= 5);
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean checkCollision(float x, BigDecimal y, float r) {
        return (x <= 0 && y.compareTo(BigDecimal.ZERO) >= 0 && y.compareTo(new BigDecimal(r)) <= 0 && x >= -r/2) ||
                (x <= 0 && y.compareTo(BigDecimal.ZERO) <= 0 && y.multiply(y).add(new BigDecimal(x*x)).compareTo(new BigDecimal(r*r)) <= 0) ||
                (x >= 0 && y.compareTo(BigDecimal.ZERO) <= 0 && y.compareTo(new BigDecimal(x - r)) >= 0);
    }
}
