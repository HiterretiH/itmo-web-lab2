package org.lab2.util;

import org.lab2.beans.Row;
import org.lab2.beans.RowsBean;

public class TableGenerator {
    public static String generateRow(Row row) {
        return (row.isHit() ? "<tr class='hit'>" : "<tr class='miss'>") + "\n" +
                "<td>" + row.getRequestTime() + "</td>\n" +
                "<td>" + row.getExecutionTime() + "</td>\n" +
                "<td>" + row.getX() + "</td>\n" +
                "<td>" + row.getY() + "</td>\n" +
                "<td>" + row.getR() + "</td>\n" +
                (row.isHit() ? "<td>+</td>" : "<td>-</td>") + "\n" +
                "</tr>\n";
    }

    public static String generateTable(RowsBean table) {
        StringBuilder tableRows = new StringBuilder();
        for (Row row : table.getRows()) {
            tableRows.append(generateRow(row));
        }

        return tableRows.toString();
    }
}
