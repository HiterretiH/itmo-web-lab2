package org.lab2.beans;

import javax.enterprise.context.SessionScoped;

import java.io.Serializable;
import java.util.Deque;
import java.util.LinkedList;

@SessionScoped
public class RowsBean implements Serializable {
    private final Deque<Row> rows;
    private final int maxLength = 10;

    public RowsBean() {
        rows = new LinkedList<>();
    }

    public void addRow(Row row) {
        rows.addFirst(row);
        if (rows.size() > maxLength) {
            rows.removeLast();
        }
    }

    public Deque<Row> getRows() {
        return rows;
    }
}
