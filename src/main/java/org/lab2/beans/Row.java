package org.lab2.beans;

import java.math.BigDecimal;

public class Row {
    private String requestTime;
    private long executionTime;
    private float x;
    private BigDecimal y;
    private float r;
    private boolean hit;

    public Row(String requestTime, long executionTime, float x, BigDecimal y, float r, boolean hit) {
        this.requestTime = requestTime;
        this.executionTime = executionTime;
        this.x = x;
        this.y = y;
        this.r = r;
        this.hit = hit;
    }

    public Row setRequestTime(String requestTime) {
        this.requestTime = requestTime;
        return this;
    }

    public Row setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
        return this;
    }

    public Row setX(float x) {
        this.x = x;
        return this;
    }

    public Row setY(BigDecimal y) {
        this.y = y;
        return this;
    }

    public Row setR(float r) {
        this.r = r;
        return this;
    }

    public Row setHit(boolean hit) {
        this.hit = hit;
        return this;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public float getX() {
        return x;
    }

    public BigDecimal getY() {
        return y;
    }

    public float getR() {
        return r;
    }

    public boolean isHit() {
        return hit;
    }
}
